/* Script written by Florian Guiffrey
   ----------------------------- v1.0
   French Version -------------------
   ----------------------------------
   ----- www.florianguiffrey.fr -----
*/

window.addEventListener('load', function(){

    var App = {
        hours : null,
        minutes : null,
        getCurrentDate : function(){
            var time = new Date();
            return time;
        },
        getTime : function(){
            return {hours: this.getCurrentDate().getHours(), minutes : this.getCurrentDate().getMinutes()};
        },
        setTime : function(){
            window.setInterval(function(){
                App.hours = App.getTime().hours;
                App.minutes = App.getTime().minutes;
                App.clear();
                App.printTime();
            }, 30000)
        },
        clear : function(){
            var cells = document.getElementsByTagName('td');
            for(var i=0; i<cells.length; i++){
                //Except IL EST
                if(i != 0 && i != 1 && i != 3 && i != 4 && i != 5)
                    cells[i].classList.remove('on');
            }
        },
        table : {
            a : ['i', 'l', 'r', 'e', 's', 't', 'i', 'u', 'n', 'e', 'p'],
            b : ['d', 'e', 'u', 'x', 'l', 'k', 't', 'r', 'o', 'i', 's'],
            c : ['q', 'u', 'a', 't', 'r', 'e', 'd', 'o', 'u', 'z', 'e'],
            d : ['c', 'i', 'n', 'q', 's', 'i', 'x', 's', 'e', 'p', 't'],
            e : ['h', 'u', 'i', 't', 'n', 'e', 'u', 'f', 'd', 'i', 'x'],
            f : ['o', 'n', 'z', 'e', 'j', 'h', 'e', 'u', 'r', 'e', 's'],
            g : ['m', 'o', 'i', 'n', 's', 'q', 'l', 'e', 'd', 'i', 'x'],
            h : ['e', 't', 's', 'q', 'u', 'a', 'r', 't', 'b', 's', 'r'],
            i : ['v', 'i', 'n', 'g', 't', '-', 'c', 'i', 'n', 'q', 'y'],
            j : ['e', 't', 'u', 'd', 'e', 'm', 'i', 'p', 'j', 'r', 'g']
        },
        displayTable : function(){
            var rowsName = Object.keys(this.table);

            rowsName.forEach(function(row, i, rows){
                for(var j=0; j<11; j++){
                    var get = document.getElementById(row + j);
                    get.textContent = App.table[row][j];
                }
            });
        },
        assocDefault : {
            'il' : function(){
                return {row: 'a', colF: 0, colL: 1}
            },
            'est' : function(){
                return {row: 'a', colF: 3, colL: 5}
            },
            'heure' : function(){
                return {row: 'f', colF: 5 , colL: 9 }
            },
            'heures' : function(){
                return {row: 'f', colF: 5, colL: 10}
            }
        },
        assocHours : {
            'une' : function(){
                return {row: 'a', colF: 7, colL: 9}
            },
            'deux' : function(){
                return {row: 'b', colF: 0, colL: 3}
            },
            'trois' : function(){
                return {row: 'b', colF: 6, colL: 10}
            },
            'quatre' : function(){
                return {row: 'c', colF: 0, colL: 5}
            },
            'cinq' : function(){
                return {row: 'd', colF: 0, colL: 3}
            },
            'six' : function(){
                return {row: 'd', colF: 4, colL: 6}
            },
            'sept' : function(){
                return {row: 'd', colF: 7, colL: 10}
            },
            'huit' : function(){
                return {row: 'e', colF: 0, colL: 3}
            },
            'neuf' : function(){
                return {row: 'e', colF: 4, colL: 7}
            },
            'dix' : function(){
                return {row: 'e', colF: 8, colL: 10}
            },
            'onze' : function(){
                return {row: 'f', colF: 0, colL: 3}
            },
            'douze' : function(){
                return {row: 'c', colF: 6, colL: 10}
            }
        },
        assocMinutes : {
            'cinq' : function(){
                return {row: 'i', colF: 6, colL: 9}
            },
            'dix' : function(){
                return {row: 'g', colF: 8, colL: 10}
            },
            'etQ' : function(){
                return {row: 'h', colF: 0, colL: 1}
            },
            'etD' : function(){
                return {row: 'j', colF: 0, colL: 1}
            },
            'quart' : function(){
                return {row: 'h', colF: 3, colL: 7}
            },
            'demi' : function(){
                return {row: 'j', colF: 3, colL: 6}
            },
            'moins' : function(){
                return {row: 'g', colF: 0, colL: 4}
            },
            'le' : function(){
                return {row: 'g', colF: 6, colL: 7}
            },
            'vingt' : function(){
                return {row: 'i', colF: 0, colL: 4}
            },
            'vingt-cinq' : function(){
                return {row: 'i', colF: 0, colL: 9}
            }

        },
        displayLettersOn : function(src, value){
            for (var i = App[src][value]().colF; i <= App[src][value]().colL; i++) {
                letter = document.getElementById(App[src][value]().row + i);
                letter.classList.add('on');
            }
        },
        printTime : function(){

            //===================//
            //      DEFAULT      //
            //===================//

            var defaultPrint = function(){
                var letter;
                //print : heure(s)
                if(App.hours == 1){
                    for (var i = App.assocDefault.heure().colF; i <= App.assocDefault.heure().colL; i++) {
                        letter = document.getElementById(App.assocDefault.heure().row + i);
                        letter.classList.add('on');
                    }
                }
                else{
                    for (var i = App.assocDefault.heures().colF; i <= App.assocDefault.heures().colL; i++) {
                        letter = document.getElementById(App.assocDefault.heures().row + i);
                        letter.classList.add('on');
                    }
                }
            }();

            //=================//
            //      HOURS      //
            //=================//

            var hoursPrint = function(){

                var value;
                var hours = App.hours;

                if(App.minutes >= 35){
                    hours = App.hours+1;
                }

                //Attributing values
                switch(hours){
                    case 1 : case 13 : value = 'une'; break;
                    case 2 : case 14 : value = 'deux'; break;
                    case 3 : case 15 : value = 'trois'; break;
                    case 4 : case 16 : value = 'quatre'; break;
                    case 5 : case 17 : value = 'cinq'; break;
                    case 6 : case 18 : value = 'six'; break;
                    case 7 : case 19 : value = 'sept'; break;
                    case 8 : case 20 : value = 'huit'; break;
                    case 9 : case 21 : value = 'neuf'; break;
                    case 10 : case 22 : value = 'dix'; break;
                    case 11 : case 23 : value = 'onze'; break;
                    case 12 : case 24 : case 0 : value = 'douze'; break;
                }
                    //We call the function to display good letters
                    App.displayLettersOn('assocHours', value);
            }();

            //===================//
            //      MINUTES      //
            //===================//

            var minutesPrint = function(){

                if(App.minutes >= 5 && App.minutes < 10){ App.displayLettersOn('assocMinutes', 'cinq'); }
                else if(App.minutes >= 10 && App.minutes < 15){ App.displayLettersOn('assocMinutes', 'dix'); }
                else if(App.minutes >= 10 && App.minutes < 15){ App.displayLettersOn('assocMinutes', 'dix'); }
                else if(App.minutes >= 15 && App.minutes < 20){ App.displayLettersOn('assocMinutes', 'etQ'); App.displayLettersOn('assocMinutes', 'quart'); }
                else if(App.minutes >= 20 && App.minutes < 25){ App.displayLettersOn('assocMinutes', 'vingt'); }
                else if(App.minutes >= 25 && App.minutes < 30){ App.displayLettersOn('assocMinutes', 'vingt-cinq'); }
                else if(App.minutes >= 30 && App.minutes < 35){ App.displayLettersOn('assocMinutes', 'etD'); App.displayLettersOn('assocMinutes', 'demi'); }
                else if(App.minutes >= 35 && App.minutes < 40){ App.displayLettersOn('assocMinutes', 'moins'); App.displayLettersOn('assocMinutes', 'vingt-cinq'); }
                else if(App.minutes >= 40 && App.minutes < 45){ App.displayLettersOn('assocMinutes', 'moins'); App.displayLettersOn('assocMinutes', 'vingt'); }
                else if(App.minutes >= 45 && App.minutes < 50){ App.displayLettersOn('assocMinutes', 'moins'); App.displayLettersOn('assocMinutes', 'le'); App.displayLettersOn('assocMinutes', 'quart'); }
                else if(App.minutes >= 50 && App.minutes < 55){ App.displayLettersOn('assocMinutes', 'moins'); App.displayLettersOn('assocMinutes', 'dix'); }
                else if(App.minutes >= 55 && App.minutes < 60){ App.displayLettersOn('assocMinutes', 'moins'); App.displayLettersOn('assocMinutes', 'cinq'); }
            }();


        },
        init : function(){
            //First time
            App.hours = App.getTime().hours;
            App.minutes = App.getTime().minutes;

            var initPrint = function() {
                var letter = '';

                //print : il
                App.displayLettersOn('assocDefault', 'il');
                //print : est
                App.displayLettersOn('assocDefault', 'est');
                //Then we can print time
                App.printTime();
            }();

            //-----
            //Then
            this.setTime();
            this.displayTable();
        }
    };

    App.init();

});